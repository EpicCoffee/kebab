package view;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.SHIP;
import model.SmallInfoLabel;

import java.util.ArrayList;
import java.util.Random;

public class GameViewManager {

    private AnchorPane gamePane;
    private Scene gameScene;
    private Stage gameStage;

    private static final int GAME_WIDTH = 600;
    private static final int GAME_HEIGHT = 800;

    private Stage menuStage;
    private ImageView ship;

    private boolean isSpacePressed;
    private boolean isUpKeyPressed;
    private boolean isDownKeyPressed;
    private boolean isLeftKeyPressed;
    private boolean isRightKeyPressed;
    private int angle;
    private AnimationTimer gameTimer;

    private GridPane gridPane1; // 12 slots in each grid
    private GridPane gridPane2;
    private final static String BACKGROUND_IMAGE_PATH ="view/resources/deep_blue.png";

    private final static String METEOR_BROWN_IMAGE_PATH = "view/resources/meteorBrown.png";
    private final static String METEOR_GREY_IMAGE_PATH = "view/resources/meteorGrey.png";

    private ImageView[] brownMeteors;
    private ImageView[] greyMeteors;
    private Random randomPositionGenerator;

    private ImageView star;
    private SmallInfoLabel pointsLabel;
    private ImageView[] platerLives;
    private int playerLives;
    private int points;
    private final static String GOLD_STAR_IMAGE_PATH = "view/resources/star_gold.png";

    private final static int STAR_RADIUS = 12;
    private final static int SHIP_RADIUS = 27;
    private final static int METEOR_RADIUS = 20;

    ArrayList<ImageView> bullets = new ArrayList<>();
    private final static String LASER_IMAGE_PATH = "view/resources/laserGreen.png";

    private ImageView bullet;
    private int gunDelay = 20;

    public GameViewManager() {
        initializeStage();
        createKeyListeners();
        randomPositionGenerator = new Random();
    }

    private void createKeyListeners() {
        gameScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.LEFT) {
                    isLeftKeyPressed = true;
                }
                else if (event.getCode() == KeyCode.RIGHT) {
                    isRightKeyPressed = true;
                }
                else if (event.getCode() == KeyCode.UP) {
                    isUpKeyPressed = true;
                }
                else if (event.getCode() == KeyCode.DOWN) {
                    isDownKeyPressed = true;
                }
                else if (event.getCode() == KeyCode.SPACE) {
                    isSpacePressed = true;
                }
            }
        });

        gameScene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.LEFT) {
                    isLeftKeyPressed = false;
                }
                else if (event.getCode() == KeyCode.RIGHT) {
                    isRightKeyPressed = false;
                }
                else if (event.getCode() == KeyCode.UP) {
                    isUpKeyPressed = false;
                }
                else if (event.getCode() == KeyCode.DOWN) {
                    isDownKeyPressed = false;
                }
                else if (event.getCode() == KeyCode.SPACE) {
                    isSpacePressed = false;
                }
            }
        });
    }

    private void initializeStage() {
        gamePane = new AnchorPane();
        gameScene = new Scene(gamePane, GAME_WIDTH, GAME_HEIGHT);
        gameStage = new Stage();
        gameStage.setScene(gameScene);
    }

    public void createNewGame(Stage menuStage, SHIP chosenShip) {
        this.menuStage = menuStage;
        this.menuStage.hide();
        createBackground();
        createShip(chosenShip);
        createGameElements(chosenShip);
        createGameLoop();
        gameStage.show();
    }

    private void createGameElements(SHIP chosenShip) {
        playerLives = 2;
        star = new ImageView(GOLD_STAR_IMAGE_PATH);
        setElementPosition(star);
        gamePane.getChildren().add(star);

        pointsLabel = new SmallInfoLabel("POINTS : 00");
        pointsLabel.setLayoutX(460);
        pointsLabel.setLayoutY(20);
        gamePane.getChildren().add(pointsLabel);
        platerLives = new ImageView[3];

        for (int i = 0; i < platerLives.length ; i++) {
            platerLives[i] = new ImageView(chosenShip.getUrlLife());
            platerLives[i].setLayoutX(455 + (i * 50 ));
            gamePane.getChildren().add(platerLives[i]);
        }

        brownMeteors = new ImageView[3];
        for (int i = 0; i < brownMeteors.length; i++) {
            brownMeteors[i] = new ImageView(METEOR_BROWN_IMAGE_PATH);
            setElementPosition(brownMeteors[i]);
            gamePane.getChildren().add(brownMeteors[i]);
        }
        greyMeteors = new ImageView[3];
        for (int i = 0; i < greyMeteors.length; i++) {
            greyMeteors[i] = new ImageView(METEOR_GREY_IMAGE_PATH);
            setElementPosition(greyMeteors[i]);
            gamePane.getChildren().add(greyMeteors[i]);
        }
    }
    
    private void moveGameElements() {
        star.setLayoutY(star.getLayoutY() + 5);

        for (int i = 0; i < brownMeteors.length; i++) {
            brownMeteors[i].setLayoutY(brownMeteors[i].getLayoutY()+7);
            brownMeteors[i].setRotate((brownMeteors[i].getRotate() +4));
        }
        for (int i = 0; i < greyMeteors.length; i++) {
            greyMeteors[i].setLayoutY(greyMeteors[i].getLayoutY()+7);
            greyMeteors[i].setRotate((greyMeteors[i].getRotate() +4));
        }

        for (int i = 0; i < bullets.size() ; i++) {
            bullets.get(i).setLayoutY(bullets.get(i).getLayoutY() - 10);
        }
    }

    private void checkIfElementsAreBehindTheShipAndRelocate() {

        if (star.getLayoutY() > 1200) {
            setElementPosition(star);
        }

        for (int i = 0; i < brownMeteors.length; i ++) {
            if (brownMeteors[i].getLayoutY() > 900) {
                setElementPosition(brownMeteors[i]);
            }
        }
        for (int i = 0; i < greyMeteors.length; i ++) {
            if (greyMeteors[i].getLayoutY() > 900) {
                setElementPosition(greyMeteors[i]);
            }
        }

        for (int i = 0; i < bullets.size() ; i++) {
            if (bullets.get(i).getLayoutY() < -5) {
                gamePane.getChildren().remove(bullets.get(i));
                bullets.remove(i);
            }
        }
    }

    private void setElementPosition(ImageView image) {
        image.setLayoutX(randomPositionGenerator.nextInt(370));
        image.setLayoutY(-(randomPositionGenerator.nextInt(3200) + 600));
    }

    private void createShip(SHIP choosenShip) {
        ship = new ImageView(choosenShip.getURL());
        ship.setLayoutX(GAME_WIDTH/2);
        ship.setLayoutY(GAME_HEIGHT - 90);
        gamePane.getChildren().add(ship);
    }

    private void createGameLoop() {
        gameTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                moveBackground();
                moveGameElements();
                checkIfElementsAreBehindTheShipAndRelocate();
                checkIfElementsCollide();
                moveShip();
                if (gunDelay > 0) {
                    gunDelay--;
                }
            }
        };

        gameTimer.start();
    }

    private void moveShip() {

        if (isSpacePressed && gunDelay < 1) {
            shoot();
            gunDelay = 30;
        }

        if (isUpKeyPressed) {
            if (ship.getLayoutY() > GAME_HEIGHT*0.5) {
                ship.setLayoutY(ship.getLayoutY() - 3);
            }
        }

        if (isDownKeyPressed) {
            if (ship.getLayoutY() < GAME_HEIGHT - 90) {
                ship.setLayoutY(ship.getLayoutY() + 3);
            }
        }

        if (isLeftKeyPressed && !isRightKeyPressed) {
            if (angle > -30) {
                angle -= 5;
            }
            ship.setRotate(angle);

            if (ship.getLayoutX() > -20) {
                ship.setLayoutX(ship.getLayoutX() - 3);
            }
        }
        if (isRightKeyPressed && !isLeftKeyPressed) {
            if (angle < 30) {
                angle += 5;
            }
            ship.setRotate(angle);

            if (ship.getLayoutX() < 522) {
                ship.setLayoutX(ship.getLayoutX() + 3);
            }
        }

        if (!isLeftKeyPressed && !isRightKeyPressed) {
            if (angle < 0) {
                angle += 5;
            }
            else if (angle > 0) {
                angle -=5;
            }
            ship.setRotate(angle);
        }

        if (isLeftKeyPressed && isRightKeyPressed) {
            if (angle < 0) {
                angle += 5;
            }
            else if (angle > 0) {
                angle -=5;
            }
            ship.setRotate(angle);
        }
    }

    private void shoot() {
        bullet = new ImageView(LASER_IMAGE_PATH);
        bullet.setLayoutY(ship.getLayoutY() - 40);
        bullet.setLayoutX(ship.getLayoutX() + 35 + angle);  // 35 is value for middle

        bullets.add(bullet);
        gamePane.getChildren().add(bullet);
    }

    private void createBackground() {
        gridPane1 = new GridPane();
        gridPane2 = new GridPane();

        for (int i = 0; i < 12 ; i++) {
            ImageView backgroundImage1 = new ImageView(BACKGROUND_IMAGE_PATH);
            ImageView backgroundImage2 = new ImageView(BACKGROUND_IMAGE_PATH);
            GridPane.setConstraints(backgroundImage1, i%3, i/3);
            GridPane.setConstraints(backgroundImage2, i%3, i/3);
            gridPane1.getChildren().add(backgroundImage1);
            gridPane2.getChildren().add(backgroundImage1);
        }

        gridPane2.setLayoutY(-1024);

        gamePane.getChildren().addAll(gridPane1, gridPane2);
    }

    private void moveBackground() {
        gridPane1.setLayoutY(gridPane1.getLayoutY() + 0.5);
        gridPane2.setLayoutY(gridPane2.getLayoutY() + 0.5);

        if (gridPane1.getLayoutY() >= 1024) {
            gridPane1.setLayoutY(-1024);
        }

        if (gridPane2.getLayoutY() >= 1024) {
            gridPane2.setLayoutY(-1024);
        }
    }

    private void checkIfElementsCollide() {
        if (SHIP_RADIUS + STAR_RADIUS > calculateDistance(ship.getLayoutX() + 49, star.getLayoutX() + 15, ship.getLayoutY() + 37, star.getLayoutY() + 15)) {
            setElementPosition(star);

            points++;
            String textToset = "POINTS : ";
            pointsLabel.setText(textToset + points);
        }

        for (int i = 0; i < brownMeteors.length ; i++) {
            if (METEOR_RADIUS + SHIP_RADIUS > calculateDistance(ship.getLayoutX() + 49, brownMeteors[i].getLayoutX() + 20, ship.getLayoutY() + 37, brownMeteors[i].getLayoutY() + 20)) {
                removeLife();
                setElementPosition(brownMeteors[i]);
            }
        }

        for (int i = 0; i < greyMeteors.length ; i++) {
            if (METEOR_RADIUS + SHIP_RADIUS > calculateDistance(ship.getLayoutX() + 49, greyMeteors[i].getLayoutX() + 20, ship.getLayoutY() + 37, greyMeteors[i].getLayoutY() + 20)) {
                removeLife();
                setElementPosition(greyMeteors[i]);
            }
        }

        bulletloop :for (int j = 0; j < bullets.size() ; j++) {
            for (int i = 0; i < brownMeteors.length ; i++) {
                if (METEOR_RADIUS + 10 > calculateDistance(bullets.get(j).getLayoutX() + 20, brownMeteors[i].getLayoutX() + 20, bullets.get(j).getLayoutY() + 20, brownMeteors[i].getLayoutY() + 20)) {
                    setElementPosition(brownMeteors[i]);
                    gamePane.getChildren().remove(bullets.get(j));
                    bullets.remove(j);
                    break bulletloop;
                }
            }
        }

        bulletloop2 : for (int j = 0; j < bullets.size() ; j++) {
            for (int i = 0; i < greyMeteors.length ; i++) {
                if (METEOR_RADIUS + 10 > calculateDistance(bullets.get(j).getLayoutX() + 20, greyMeteors[i].getLayoutX() + 20, bullets.get(j).getLayoutY() + 20, greyMeteors[i].getLayoutY() + 20)) {
                    setElementPosition(greyMeteors[i]);
                    gamePane.getChildren().remove(bullets.get(j));
                    bullets.remove(j);
                    break bulletloop2;
                }
            }
        }
    }

    private void removeLife() {
        gamePane.getChildren().remove(platerLives[playerLives]);
        playerLives--;
        if (playerLives < 0) {
            gameStage.close();
            gameTimer.stop();
            menuStage.show();
        }
    }

    private double calculateDistance(double x1, double x2 , double y1, double y2) {
        return Math.sqrt(Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2));
    }
}
