package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import model.*;

import java.util.ArrayList;
import java.util.List;

public class ViewManager {

    private static final int HEIGHT = 768;
    private static final int WIDTH = 1024;

    private AnchorPane mainPane;
    private Scene mainScene;
    private Stage mainStage;

    private final static int MENU_BUTTONS_START_X = 100;
    private final static int MENU_BUTTONS_START_Y = 150;
    private static final int LOGOX = 400;
    private static final int LOGOY = 50;

    List<SpaceInvaderButton> menuButtons;

    private SpaceInvaderSubscene creditsSubScene;
    private SpaceInvaderSubscene helpSubScene;
    private SpaceInvaderSubscene scoreSubScene;
    private SpaceInvaderSubscene shipChooseSubScene;

    private SpaceInvaderSubscene sceneToHide;

    List<ShipPicker> shipList;
    private SHIP choosenShip;

    public ViewManager() {
        menuButtons = new ArrayList<>();
        mainPane = new AnchorPane();
        mainScene = new Scene(mainPane, WIDTH, HEIGHT);
        mainStage = new Stage();
        mainStage.setScene(mainScene);

        createSubScenes();
        createButton();
        createBackground();
        //createLogo();

    }

    private void showSubScene(SpaceInvaderSubscene subScene) {
        if (sceneToHide != null) {
            sceneToHide.moveSubScene();
        }

        subScene.moveSubScene();
        sceneToHide = subScene;
    }

    private void createSubScenes () {
        creditsSubScene = new SpaceInvaderSubscene();
        mainPane.getChildren().add(creditsSubScene);

        helpSubScene = new SpaceInvaderSubscene();
        mainPane.getChildren().add(helpSubScene);

        scoreSubScene = new SpaceInvaderSubscene();
        mainPane.getChildren().add(scoreSubScene);

        createShipChooserSubScene();

    }

    private void createShipChooserSubScene() {
        shipChooseSubScene = new SpaceInvaderSubscene();
        mainPane.getChildren().add(shipChooseSubScene);

        InfoLabel chooseShipLabel = new InfoLabel("CHOOSE YOUR SHIP");
        chooseShipLabel.setLayoutX(110);
        chooseShipLabel.setLayoutY(25);
        shipChooseSubScene.getPane().getChildren().add(chooseShipLabel);
        shipChooseSubScene.getPane().getChildren().add(createShipsToChoose());
        shipChooseSubScene.getPane().getChildren().add(createButtonToStart());

    }

    private HBox createShipsToChoose() {
        HBox box = new HBox();
        box.setSpacing(20);
        shipList = new ArrayList<>();
        for (SHIP ship : SHIP.values()) {
            ShipPicker shipToPick = new ShipPicker(ship);
            shipList.add(shipToPick);
            box.getChildren().add(shipToPick);
            shipToPick.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    for (ShipPicker ship : shipList) {
                        ship.setCircleChoosen(false);
                    }
                    shipToPick.setCircleChoosen(true);
                    choosenShip = shipToPick.getShip();
                }
            });
        }
        box.setLayoutX(300 - (118*2));
        box.setLayoutY(100);
        return box;
    }

    public Stage getMainStage()  {
        return  mainStage;
    }

    private  SpaceInvaderButton createButtonToStart() {
        SpaceInvaderButton startButton = new SpaceInvaderButton("START");
        startButton.setLayoutX(350);
        startButton.setLayoutY(300);

        startButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(choosenShip != null) {
                   GameViewManager gameManager = new GameViewManager();
                   gameManager.createNewGame(mainStage, choosenShip);
                }
            }
        });

        return startButton;
    }

    private void addMenuButton(SpaceInvaderButton button) {
        button.setLayoutX(MENU_BUTTONS_START_X);
        button.setLayoutY(MENU_BUTTONS_START_Y + menuButtons.size() * 100);
        menuButtons.add(button);
        mainPane.getChildren().add(button);
    }

    private void  createButton() {
    /*    String[] menuTexts = {"PLAY", "SCORES", "HELP", "CREDITS", "EXIT"};
        for (int i = 0; i < menuTexts.length; i++) {
            addMenuButton(new SpaceInvaderButton(menuTexts[i]));
        } */
    createPlayButton();
    createScoreButton();
    createHelpButton();
    createCreditsButton();
    createExitButton();
    }

   private void createPlayButton() {
       SpaceInvaderButton play = new SpaceInvaderButton("PLAY");
       addMenuButton(play);

       play.setOnAction(new EventHandler<ActionEvent>() {
           @Override
           public void handle(ActionEvent event) {
               showSubScene(shipChooseSubScene);
           }
       });
    }

    private void createScoreButton() {
        SpaceInvaderButton score = new SpaceInvaderButton("SCORE");
        addMenuButton(score);
        score.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showSubScene(scoreSubScene);
            }
        });
    }
    private void createHelpButton() {
        SpaceInvaderButton help = new SpaceInvaderButton("HELP");
        addMenuButton(help);
        help.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showSubScene(helpSubScene);
            }
        });
    }
    private void createCreditsButton() {
        SpaceInvaderButton credits = new SpaceInvaderButton("CREDITS");
        addMenuButton(credits);

        credits.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showSubScene(creditsSubScene);
            }
        });
    }
    private void createExitButton() {
        SpaceInvaderButton exit = new SpaceInvaderButton("EXIT");
        addMenuButton(exit);

        exit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainStage.close();
            }
        });
    }


    private void createBackground() {

        Image deepBlue = new Image("view/resources/deep_blue.png", 256, 256, false, true);
        BackgroundImage background = new BackgroundImage(deepBlue, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, null);
        mainPane.setBackground(new Background(background));
    }

    private void createLogo() {
        ImageView logo = new ImageView("view/resources/logo.png");
        logo.setLayoutX(LOGOX);
        logo.setLayoutX(LOGOY);

        logo.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                logo.setEffect(new DropShadow());
            }
        });

        logo.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                logo.setEffect(null);
            }
        });
        mainPane.getChildren().add(logo);
    }
}
