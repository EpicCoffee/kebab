package model;

public enum SHIP {
    BLUE("view/resources/shipchooser/playerShip_blue.png", "view/resources/shipchooser/playerLife_blue.png"),
    GREEN("view/resources/shipchooser/playerShip_green.png", "view/resources/shipchooser/playerLife_green.png"),
    ORANGE("view/resources/shipchooser/playerShip_orange.png", "view/resources/shipchooser/playerLife_orange.png"),
    RED("view/resources/shipchooser/playerShip_red.png", "view/resources/shipchooser/playerLife_red.png");

    private String urlShip;
    private String urlLife;

    private SHIP(String urlShip, String urlLife) {

        this.urlShip = urlShip;
        this.urlLife = urlLife;
    }

    public String getURL() {
        return this.urlShip;
    }

    public String getUrlLife() {
        return urlLife;
    }
}
